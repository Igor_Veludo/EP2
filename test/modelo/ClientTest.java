package modelo;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

public class ClientTest {
	
	static Client client;
	
	@BeforeClass
	public static void testSetup(){
		client = new Client();
	}

	@Test
	public void testName() {
		client.setName("Igor");
		assertTrue(client.getName().equalsIgnoreCase("igor"));
	}
	
	@Test
	public void testRg() {
		client.setRg("12345");
		assertTrue(client.getRg().equalsIgnoreCase("12345"));
	}
	

}
