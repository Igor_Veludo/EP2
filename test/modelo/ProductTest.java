package modelo;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

public class ProductTest {
	
	static Product product;
	
	@BeforeClass
	public static void testSetup(){
		product = new Product();
	}

	@Test
	public void testName() {
		product.setName("feijao");
		assertTrue(product.getName().equalsIgnoreCase("feijao"));
	}
	
	@Test
	public void testPrice() {
		product.setPrice(12345);
		assertTrue(product.getPrice() == 12345);
	}
	
	@Test
	public void testCod() {
		product.setCodigo(12345);
		assertTrue(product.getCodigo() == 12345);
	}
	

}
