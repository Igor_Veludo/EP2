package modelo;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

public class StockTest {
	
	static Stock stock;
	
	@BeforeClass
	public static void testSetup(){
		stock = new Stock();
	}

	@Test
	public void testName() {
		stock.setMount(10);
		assertTrue(stock.getMount() == 10);
	}
	
	@Test
	public void testMountMin() {
		stock.setMountMin(12345);
		assertTrue(stock.getMountMin() == 12345);
	}
	
	@Test
	public void testCod(){
		stock.setCod(10);
		assertTrue(stock.getCod() == 10);
	}

}
