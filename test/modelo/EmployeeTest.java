package modelo;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

public class EmployeeTest {
	
	static Employee employee;
	
	@BeforeClass
	public static void testSetup(){
		employee = new Employee();
	}

	@Test
	public void testName() {
		employee.setName("felipe");
		assertTrue(employee.getName().equalsIgnoreCase("felipe"));
	}
	
	@Test
	public void testRg() {
		employee.setRg("12345");
		assertTrue(employee.getRg().equals("12345"));
	}
	
	@Test
	public void testLogin() {
		employee.setLogin("adm");
		assertTrue(employee.getLogin().equals("adm"));
	}
	
	@Test
	public void testPassword() {
		employee.setPassword("adm");
		assertTrue(employee.getPassword().equals("adm"));
	}
	

}
