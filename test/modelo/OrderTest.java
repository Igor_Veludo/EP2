package modelo;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

public class OrderTest {
	
	static Order order;
	
	@BeforeClass
	public static void testSetup(){
		order = new Order();
	}

	@Test
	public void testName() {
		order.setTotalValue(100);
		assertTrue(order.getTotalValue() == 100);
	}
	
	@Test
	public void testObservation() {
		order.setObservation("Gosto mal passado");
		assertTrue(order.getObservation().equals("Gosto mal passado"));
	}
}
