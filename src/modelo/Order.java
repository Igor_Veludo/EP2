package modelo;

import java.util.ArrayList;
import java.util.Calendar;

import controle.Payment;

public class Order {
	private Calendar date;
	private float totalValue;
	private ArrayList<Product> products;
	private String observation;
	
	public Order(){
		
	}
	
	public Calendar getDate() {
		return date;
	}
	public void setDate(Calendar date) {
		this.date = date;
	}
	public float getTotalValue() {
		return totalValue;
	}
	public void setTotalValue(float totalValue) {
		this.totalValue = totalValue;
	}

	public ArrayList<Product> getProducts() {
		return products;
	}

	public void setProducts(ArrayList<Product> products) {
		this.products = products;
	}

	public String getObservation() {
		return observation;
	}

	public void setObservation(String observation) {
		this.observation = observation;
	}
	
	
	
	
}
