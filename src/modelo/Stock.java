package modelo;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Stock {
	private Integer mount;
	private Integer mountMin;
	private Integer cod;
	
	public Stock(){
		
	}

	public Integer getMount() {
		return mount;
	}

	public void setMount(Integer mount) {
		this.mount = mount;
	}


	public Integer getMountMin() {
		return mountMin;
	}

	public void setMountMin(Integer mountMin) {
		this.mountMin = mountMin;
	}

	public Integer getCod() {
		return cod;
	}

	public void setCod(Integer cod) {
		this.cod = cod;
	}
	
	
	
}
