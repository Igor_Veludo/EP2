package visao;

import java.awt.BorderLayout;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import modelo.Client;

public class ClientView extends JPanel {

  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

JList<String> list;

  DefaultListModel<String> model;

  public ClientView(ArrayList<Client> clients) {
	setLayout(new BorderLayout());
    model = new DefaultListModel<String>();
    list = new JList<String>(model);
    JScrollPane pane = new JScrollPane(list);
    
    model.addElement("NOME, RG");
    for(Client client: clients){
		model.addElement(client.getName()+", "+client.getRg());
    }


    add(pane, BorderLayout.NORTH);

  } 
}