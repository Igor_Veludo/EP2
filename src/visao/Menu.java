package visao;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.WindowConstants;

import controle.Crud;
public class Menu extends JFrame{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JMenuBar BarraMenu = null;
	private JMenu mnuArquivo = null;
	private JMenu mnuClients = null;
	private JMenu mnuOrder = null;
	private JMenuItem setProduct = null;
	private JMenuItem listProduct = null;
	private JMenuItem setClient = null;
	private JMenuItem listClient = null;
	private JMenuItem fazerPedido = null;
	private JMenuItem fecharConta = null;
	
	
	Crud crud = new Crud();
	public Menu() {
		super();
	}
	public void initialize() throws IOException{
		JFrame frame = new JFrame();
		BufferedImage image = null;
		String path = "image.png";
        image = ImageIO.read(new File(path));
        JLabel label = new JLabel(new ImageIcon(image));
        
		frame.setTitle("Restaurante tô com fome");
		frame.setJMenuBar(getBarraMenu());
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.getContentPane().add(label);
		frame.pack();
		frame.setSize(1280,410);
		frame.setVisible(true);
		frame.add(new JLabel(new ImageIcon("../../image.png")));
		
		//Caso este item seja escolhido..
		setProduct.addActionListener(new java.awt.event.ActionListener() {
		    public void actionPerformed(java.awt.event.ActionEvent evt) {
		        try {
					crud.storage(evt);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    }
		});
		
		//Caso este item seja escolhido..
		listProduct.addActionListener(new java.awt.event.ActionListener() {
		    public void actionPerformed(java.awt.event.ActionEvent evt) {
		        crud.listProducts(evt);
		    }
		});
		
		//Caso este item seja escolhido..
		setClient.addActionListener(new java.awt.event.ActionListener() {
		    public void actionPerformed(java.awt.event.ActionEvent evt) {
		        crud.storageClient(evt);
		    }
		});
		
		//Caso este item seja escolhido..
		listClient.addActionListener(new java.awt.event.ActionListener() {
		    public void actionPerformed(java.awt.event.ActionEvent evt) {
		        crud.listClients(evt);
		    }
		});
		
		//Caso este item seja escolhido..
		fazerPedido.addActionListener(new java.awt.event.ActionListener() {
		    public void actionPerformed(java.awt.event.ActionEvent evt) {
		        crud.order(evt);
		    }
		});
		
		//Caso este item seja escolhido..
		fecharConta.addActionListener(new java.awt.event.ActionListener() {
		    public void actionPerformed(java.awt.event.ActionEvent evt) {
		        crud.closeAccount(evt);
		    }
		});
		
	}
	//Construindo a barra do menu
	public JMenuBar getBarraMenu() {
		if (BarraMenu == null){
			BarraMenu = new JMenuBar();
			BarraMenu.add(getMnuArquivo());
			BarraMenu.add(getMnuClients());
			BarraMenu.add(getMnuOrders());
		}
		return BarraMenu;
	}
	public JMenu getMnuArquivo() {
		if (mnuArquivo == null){
			mnuArquivo = new JMenu();
			mnuArquivo.setText("Produtos");
			mnuArquivo.add(getMnuSetProduct());
			mnuArquivo.add(getMnuListProduct());
		}
		return mnuArquivo;
	}
	
	public JMenu getMnuClients() {
		if (mnuClients == null){
			mnuClients = new JMenu();
			mnuClients.setText("Clientes");
			mnuClients.add(getMnuSetClient());
			mnuClients.add(getMnuListClient());
		}
		return mnuClients;
	}
	
	public JMenu getMnuOrders() {
		if (mnuOrder == null){
			mnuOrder = new JMenu();
			mnuOrder.setText("Pedidos");
			mnuOrder.add(getMnuFazerPedido());
			mnuOrder.add(getMnuFecharConta());
		}
		return mnuOrder;
	}
	
	public JMenuItem getMnuSetClient() {
		if (setClient == null){
			setClient = new JMenuItem();
			setClient.setText("Cadastrar");
		}
		return setClient;
	}
	
	public JMenuItem getMnuListClient() {
		if (listClient == null){
			listClient = new JMenuItem();
			listClient.setText("Listar");
		}
		return listClient;
	}
	
	
	public JMenuItem getMnuSetProduct() {
		if (setProduct == null){
			setProduct = new JMenuItem();
			setProduct.setText("Cadastrar");
		}
		return setProduct;
	}
	public JMenuItem getMnuListProduct() {
		if (listProduct == null){
			listProduct = new JMenuItem();
			listProduct.setText("Listar");
		}
		return listProduct;
	}
	
	public JMenuItem getMnuFazerPedido() {
		if (fazerPedido == null){
			fazerPedido = new JMenuItem();
			fazerPedido.setText("Registrar pedido");
		}
		return fazerPedido;
	}
	
	public JMenuItem getMnuFecharConta() {
		if (fecharConta == null){
			fecharConta = new JMenuItem();
			fecharConta.setText("Fechar conta");
		}
		return fecharConta;
	}
	
}