package visao;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import controle.Crud;
import modelo.Product;
import modelo.Stock;

public class ProductView extends JPanel {

  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

JList<String> list;

  DefaultListModel<String> model;

  public ProductView(ArrayList<Product> products, ArrayList<Stock> stockList) {
	setLayout(new BorderLayout());
    model = new DefaultListModel<String>();
    list = new JList<String>(model);
    JScrollPane pane = new JScrollPane(list);
    
    model.addElement("NOME, CODIGO, QUANTIDADE, MINIMO");
    for(Product product: products){
    	for(Stock stock: stockList){
    		if(stock.getCod() == product.getCodigo()){
    				model.addElement(product.getName()+", "+ stock.getCod()+", "+ stock.getMount()+", "+stock.getMountMin());
    		}
    	}
    	
    }


    add(pane, BorderLayout.NORTH);

  } 
}