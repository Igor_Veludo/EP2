package visao;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import controle.CashPayment;
import controle.CreditCardPayment;
import modelo.Client;
import modelo.Product;

public class ExtractView extends JPanel {

  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

JList<String> list;

  DefaultListModel<String> model;

  public ExtractView(ArrayList<Product> products, Client client, float totalValue, JFrame frame) {
	setLayout(new BorderLayout());
    model = new DefaultListModel<String>();
    CashPayment cash = new CashPayment();
    CreditCardPayment card = new CreditCardPayment();
    list = new JList<String>(model);
    JButton addButton = new JButton("Dinheiro");
    JButton addButtonCard = new JButton("Cartão");
    JScrollPane pane = new JScrollPane(list);
    model.addElement("Cliente: "+client.getName());
    model.addElement("PRODUTO, PREÇO");
    for(Product product: products){
		model.addElement(product.getName()+", "+product.getPrice());
    }
    
    model.addElement("Total: "+totalValue);
    
    addButton.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
          while(!cash.pay(totalValue));
          
          frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
 	
        }
      });
    addButtonCard.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
          card.pay(totalValue);
          frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
 	
        }
      });

    add(pane, BorderLayout.NORTH);
    add(addButton, BorderLayout.WEST);
    add(addButtonCard, BorderLayout.EAST);

  } 
}