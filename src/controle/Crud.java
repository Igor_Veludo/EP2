package controle;

import java.io.IOException;
import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import modelo.Client;
import modelo.Employee;
import modelo.Order;
import modelo.Product;
import modelo.Stock;
import visao.ClientView;
import visao.ExtractView;
import visao.ProductView;

public class Crud {
	
	//Registro de todos os cadastros realizados
	ArrayList<Client> clientList = new ArrayList<Client>();
	ArrayList<Employee> employeeList = new ArrayList<Employee>();
	ArrayList<Stock> stockList = new ArrayList<Stock>();
	ArrayList<Product> productList = new ArrayList<Product>();
	
	public void storage(java.awt.event.ActionEvent evt) throws IOException {
		String codigo;
		String name;
		String price;
		int mount;
		boolean stockExists = false;
		
		Product product = new Product();
	    
		
	    codigo = JOptionPane.showInputDialog("Insira o código do produto.");
	    if(codigo == null){//Se o usuário clicou em cancelar..
	    	return;
	    }else{
	    	product.setCodigo(Integer.parseInt(codigo));
	    }
	    
	    for(Stock stock: stockList){
	    	if(stock.getCod() == product.getCodigo()){
	    		stockExists = true;
	    		mount = stock.getMount();
	    		stock.setMount(mount+1);
	    		return;
	    	}
	    }
	    
	    name = JOptionPane.showInputDialog("Insira o nome do produto.");
	    if(name == null){//Se o usuário cancelou..
	    	return;
	    }else{
	    	product.setName(name);
	    }
	    
	    price = JOptionPane.showInputDialog("Insira o valor do produto.");
	    if(price == null){//Se o usuário cancelou
	    	return;
	    }else{
	    	product.setPrice(Float.parseFloat(price));
	    }
	    
	    //Depois que o usuário entrou com os dados do produto, preciso atualizar o estoque
	    //do mesmo.
	    
	    if(!stockExists){//Caso o estoque deste produto ainda n tenha sido criado:
	    	Stock stock = new Stock();
	    	stock.setCod(product.getCodigo());
	    	int minStock = Integer.parseInt(JOptionPane.showInputDialog("Qual a quantidade mínima necessária no estoque?"));
	    	stock.setMountMin(minStock);
	    	stock.setMount(1);
	    	JOptionPane.showMessageDialog(null, "Produto Cadastrado!\nNome:"+ product.getName()+"\nCodigo:" + product.getCodigo() + "\nQuantidade"+stock.getMount() + "\nMinimo exigido: "+ stock.getMountMin());
		    stockList.add(stock);
	    }
	    productList.add(product);
	   
	}
	
	public void listProducts(java.awt.event.ActionEvent evt) {
		JFrame frame = new JFrame("Listagem de produtos");
		frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
	    frame.setContentPane(new ProductView(productList, stockList));
	    frame.setSize(260, 200);
	    frame.setVisible(true);  
	}
	
	public void listClients(java.awt.event.ActionEvent evt) {
		JFrame frame = new JFrame("Listagem de clientes");
		frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
	    frame.setContentPane(new ClientView(clientList));
	    frame.setSize(260, 200);
	    frame.setVisible(true);  
	    
	}
	
	public void changeMinStock(java.awt.event.ActionEvent evt) throws IOException{
		int min;
		int codigo;
		codigo = Integer.parseInt(JOptionPane.showInputDialog("Digite o codigo do produto:"));
		min = Integer.parseInt(JOptionPane.showInputDialog("Digite o novo mínimo:"));
		for(Stock stock: stockList){
			if(stock.getCod() == codigo){
				stock.setMountMin(min);
			}
		}
	}
	
	//Clientes:
	public void storageClient(java.awt.event.ActionEvent evt){
		Client client = new Client();
		String name;
		String rg;
		name = JOptionPane.showInputDialog("Nome do cliente:");
		rg = JOptionPane.showInputDialog("RG do cliente:");
		client.setName(name);
		client.setRg(rg);
		JOptionPane.showMessageDialog(null, "Cliente cadastrado com sucesso!\nNome: "+ client.getName()+"\nRG: "+client.getRg());
		clientList.add(client);
	}
	
	//Pedidos:
	public void order(java.awt.event.ActionEvent evt){
		String codigoProduto;
		String rg;
		int option;
		float totalValue = 0;
		boolean find = false;
		boolean findClient = false;
		ArrayList<Product> products = new ArrayList<Product>();
		ArrayList<Order> ordersClient = new ArrayList<Order>();
		Order order = new Order();
		rg = JOptionPane.showInputDialog("Entre com o RG do cliente.");
		
		for(Client client: clientList){
			if(client.getRg().equals(rg)){
				if(client.getOrders() != null){
					ordersClient = client.getOrders();
				}
				do{
					codigoProduto = JOptionPane.showInputDialog("Entre com o código do produto");
					for(Product product: productList){
						if(product.getCodigo() == Integer.parseInt(codigoProduto)){
							for(Stock stock : stockList){
								if(stock.getCod() == product.getCodigo()){
									if(stock.getMount() < stock.getMountMin()){
										JOptionPane.showMessageDialog(null, "Produto indisponível, em falta no estoque.");
										return;
									}
								}
							}
							products.add(product);
							find = true;
							totalValue = totalValue + product.getPrice();
						}
					}
					if(!find){
						JOptionPane.showMessageDialog(null, "Erro! Produto não cadastrado.");
					}
					findClient = true;
					option = JOptionPane.showConfirmDialog(null, "Deseja inserir outro produto no pedido?","", JOptionPane.YES_NO_OPTION);
				}while(option == JOptionPane.YES_OPTION);
				
				order.setObservation(JOptionPane.showInputDialog("Observação:"));
				order.setDate(Calendar.getInstance());
				order.setProducts(products);
				order.setTotalValue(totalValue);
				ordersClient.add(order);
				client.setOrders(ordersClient);
				
			}
		}
		
		if(!findClient){
			JOptionPane.showMessageDialog(null, "Erro! Cliente não cadastrado!");
		}
		
		
		
	}
	
	public void closeAccount(java.awt.event.ActionEvent evt){
		String rg;
		float totalValue = 0;
		ArrayList<Order> clientOrders = new ArrayList<Order>();
		ArrayList<Product> productsOrders = new ArrayList<Product>();
		Client person = new Client();
		
		rg = JOptionPane.showInputDialog("Entre com o RG do cliente:");
		for(Client client : clientList){
			if(client.getRg().equals(rg)){//Se encontrar o cliente
				clientOrders = client.getOrders();
				person = client;
				for(Order order : clientOrders){
					totalValue = totalValue + order.getTotalValue();
					productsOrders.addAll(order.getProducts());
				}
			}
		}
		JFrame frame = new JFrame("Conta");
		frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
	    frame.setContentPane(new ExtractView(productsOrders, person, totalValue, frame));
	    frame.setSize(260, 200);
	    frame.setVisible(true);
	}
 
}
