package controle;

import javax.swing.JOptionPane;

public class CashPayment {
	public boolean pay(float totalValue){
		float value = Float.parseFloat(JOptionPane.showInputDialog("Pagamento em dinheiro, entre com o valor recebido:"));
		if(totalValue < value){
			JOptionPane.showMessageDialog(null, "Troco: " +(value-totalValue));
			return true;
		}else{
			
			JOptionPane.showMessageDialog(null, "Valor insuficiente.");
			return false;
		}
		
	}
}
